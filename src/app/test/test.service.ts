import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor(private http : HttpClient) { }

  getStudents(){
    return this.http.get<any>(`http://localhost:8000/api/students/list`)
  }

  deleteStudent(deleteID){
    console.log(deleteID)
    return this.http.post<any>(`http://localhost:8000/api/students/delete`,{deleteID})
  }

  createStudent(record){
    return this.http.post<any>(`http://localhost:8000/api/students/create`,record)
  }

  updateStudent(record){
    return this.http.post<any>(`http://localhost:8000/api/students/update`,record)
  }

}
