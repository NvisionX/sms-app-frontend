import { Component, OnInit } from '@angular/core';
import { TestService } from "./test.service";
import { FormControl, FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  studentForm : FormGroup;
  students;

  constructor(
    private testservice : TestService,
    private fb: FormBuilder
  ) {
      this.studentForm = this.fb.group({
        name : [],
        email : [],
        contact : [],
        class : [],
        section : []
      })
   }

  ngOnInit() {
    console.log('nginit of test component')
    this.getRecord()
  }

  deleteRecord(deleteID){
    console.log(deleteID)
    this.testservice.deleteStudent(deleteID)
    .subscribe(data=>{
      this.getRecord()
    })
  }

  getRecord(){
    this.testservice.getStudents()
    .subscribe(data=>{
      this.students=data
      console.log(this.students)
    })
  }

  createRecord(){
    this.testservice.createStudent(this.studentForm.value)
    .subscribe(data=>{
      this.getRecord()
    })
  }

}
